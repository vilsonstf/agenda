import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component } from '@angular/core';


export class Produto {
  constructor(public descricao: string, public preco: number, public quantidade: number) {

  }

  getVlTotal(): number {   
    return this.quantidade * this.preco;

  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'agenda';

  produtos: Produto[] = [];

  produto: Produto = new Produto('', 0, 0);

  add() {
    this.produtos.push(this.produto);
    this.produto = new Produto('', 0, 0);
  }



  getTotal(): number {
    let retorno = 0;
    for (let p of this.produtos) {
      retorno += Number(p.preco) * Number(p.quantidade);
    }
    return retorno;
  }


}


